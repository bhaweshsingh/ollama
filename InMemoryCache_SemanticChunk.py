import gradio as gr
import markdown, datetime
from datetime import date, timedelta
from bs4 import BeautifulSoup
import langchain
from langchain_core.globals import set_llm_cache
from langchain_core.caches import InMemoryCache
from langchain_community.document_loaders import TextLoader
from langchain_community.vectorstores import FAISS
from langchain.chains import RetrievalQA
from langchain_experimental.text_splitter import SemanticChunker
from langchain_huggingface import HuggingFaceEmbeddings
from langchain_openai import OpenAI
from langchain.chat_models import init_chat_model
from langchain_community.document_loaders import PyPDFLoader

filePath = "C:/Cognizant/multi-agent-ai-langgraph/docs/"
pngPath = "C:/Cognizant/multi-agent-ai-langgraph/img"

llm_options = ["gpt-4o-mini", "gpt-4o", "azure-4o-mini", "local-llama", "claude-haiku", "gemini-flash"]

# Configure Langchain caching
#langchain.llm_cache = FAISSCache.create(vectorstore, embeddings)
set_llm_cache(InMemoryCache())

# Initialize LLM and RAG chain

def run_show_markdown(Keyword, model):

    currTime1 = datetime.datetime.now()
    llm_model = llm_options[model]
    print(f"user selected LLM model: {llm_model}")

    file = f"all_research_data.md"
    with open(file, "r", encoding="utf-8") as f:
        markdown_content = f.read()

    with open(file, "r", encoding="utf-8") as f:
        markdown_content = f.read()
    html = markdown.markdown(markdown_content)
    currTime2 = datetime.datetime.now()
    time_diff = currTime2 - currTime1
    print(f"----------")
    print(f"Time taken(in sec): {time_diff.total_seconds()}")
    print(f"----------")
    return html

def display_markdown(filename):
    with open(filename, "r", encoding="utf-8") as f:
        markdown_content = f.read()
        return markdown_content 

# Function to get response using semantic cache
def get_cached_response(rag, query):
    return rag.invoke(query)

def init_RAG(model):

    print(f"----------")
    print(f"init RAG")
    print(f"----------")
    llm_model = llm_options[model]
    embed_model_name="BAAI/bge-base-en-v1.5"
    filename = "C:/BKS/Bhawesh Singh - Senior GenAI.pdf"

#    filename = "test.txt"
# Initialize embeddings and semantic chunker
    embeddings = HuggingFaceEmbeddings(model_name=embed_model_name)
# Initialize SemanticChunker with OpenAI embeddings
    text_splitter = SemanticChunker(embeddings)
# Load PDF document
    loader = PyPDFLoader(filename)
    documents = loader.load()
# Split documents into semantic chunks
    chunks = text_splitter.split_documents(documents)

# Create vectorstore and retriever
    print("now loading the file in vector store")
    vectorstore = FAISS.from_documents(chunks, embeddings)
    retriever = vectorstore.as_retriever()
    print(f'llm_model: {llm_model}')
    llm = init_chat_model(llm_model, model_provider="openai")
    rag = RetrievalQA.from_chain_type(llm=llm, chain_type="stuff", retriever=retriever)
    print(f"----------")
    print(f"DONE")
    print(f"----------")
    return rag, "RAG is initialized!"

def ask_RAG(rag, query, history):
    currTime1 = datetime.datetime.now()
    print(f"----------")
    print("running ask RAG")
    print(f"----------")

    if rag:
        try:
            print(f"response0:")
            response = get_cached_response(rag, query)
#            print(f"response: {response}")
            answer = response["result"]
        except:
            answer = "your query didn't bring any useful information."
    else:
        answer = "initilize the Chatbot first and then ask you question"
    print(f"query: {query}")
    print(f"answer: {answer}")
    new_history = history + [(query, answer)]
    print(f"----------")
    print(f"DONE")
    print(f"----------")
    currTime2 = datetime.datetime.now()
    time_diff = currTime2 - currTime1
    print(f"----------")
    print(f"Time taken(in sec): {time_diff.total_seconds()}")
    print(f"----------")
    return rag, gr.update(value=""), new_history


with gr.Blocks() as demo:

    rag = gr.State()
    gr.Markdown(
            '''
        # <center>Scientific Research Portal<center>
        ''')    
    gr.HTML(markdown.markdown(display_markdown("Overview.md")))

    with gr.Row():
        llm_btn = gr.Radio(llm_options, label="Available LLMs", value = llm_options[0], type="index") 

    with gr.Row():
        with gr.Column(scale=1):
            with gr.Group():
                with gr.Row():
                    Keyword = gr.Textbox(label="Search", value="Cancer")
                    submit_btn = gr.Button("Search")
        with gr.Column(scale=2):
            output = gr.HTML()
            chatbot = gr.Chatbot(height=305)
            with gr.Row():
                    qa_init_btn = gr.Button("Initialize Chatbot")
            with gr.Row():
                    llm_progress = gr.Textbox(value="Not initialized", show_label=False)
            with gr.Row():
                    msg = gr.Textbox(placeholder="Ask a question", container=True)
            with gr.Row():
                    qa_btn = gr.Button("Submit")
#                    clear_btn = gr.ClearButton([msg, chatbot], value="Clear")

        # Preprocessing events
    submit_btn.click(fn=run_show_markdown, inputs=[Keyword, llm_btn], outputs=output, api_name="run_show_markdown")

        # Chatbot events
    qa_init_btn.click(init_RAG, \
            inputs=[llm_btn], \
            outputs=[rag, llm_progress], \
            queue=False)
    msg.submit(ask_RAG, \
            inputs=[rag, msg, chatbot], \
            outputs=[rag, msg, chatbot], \
            queue=False)
    qa_btn.click(ask_RAG, \
            inputs=[rag, msg, chatbot], \
            outputs=[rag, msg, chatbot], \
            queue=False)
#    clear_btn.click(lambda:[None],inputs=None,outputs=[chatbot],queue=False)


#demo.launch()
#server_port=8080, 
demo.launch(share=False, server_name='0.0.0.0', server_port=7860, allowed_paths=["./", f"{pngPath}", f"{filePath}"])

